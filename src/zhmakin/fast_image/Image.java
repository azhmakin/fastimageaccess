package zhmakin.fast_image;

public interface Image
{
    int getRGB(int x, int y);
    void setRGB(int x, int y, int rgb);
}

