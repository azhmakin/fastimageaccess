package zhmakin.fast_image;

import java.util.Random;

public class Main
{
    interface Test
    {
        void run(Image image);
        String getName();
    }

    private final static Test randomAccessTest = new Test()
    {
        @Override
        public void run(Image image)
        {
            Random random = new Random(0);

            int rgb = 0;

            for (int i = 1_000_000; --i >= 0; )
            {
                if (i % 2 == 0)
                {
                    image.setRGB(random.nextInt(1024), random.nextInt(1024), rgb);
                }
                else
                {
                    rgb = image.getRGB(random.nextInt(1024), random.nextInt(1024));
                }
            }
        }

        @Override
        public String getName()
        {
            return "Random access";
        }
    };


    private final static Test linearAccessTest = new Test()
    {
        @Override
        public void run (Image image)
        {
            int rgb;

            for (int i = 10; --i >= 0; )
            {
                for (int y = 0; y < 1024; y++)
                {
                    for (int x = 0; x < 1024; x++)
                    {
                        rgb = image.getRGB(x, y);
                        rgb++;
                        image.setRGB(x, y, rgb);
                    }
                }
            }
        }

        @Override
        public String getName()
        {
            return "Linear access";
        }
    };


    static long timeTest(Test test, Image image)
    {
        long start = System.nanoTime();
        test.run(image);
        long end = System.nanoTime();

        return end - start;
    }

    public static void main(String[] args)
    {
        Image[] images = new Image[] { new BIImage(), new ImageArray(1024, 1024), new ImageArray2DYX(), new ImageArray2DXY() };
        Test[] tests = new Test[] { randomAccessTest, linearAccessTest };

        System.out.print("JIT pass...");
        // Let JIT do its job
        for (int i = 20; --i >= 0;)
        {
            for (Test test : tests)
            {
                for (Image img : images)
                {
                    timeTest(test, img);
                }
            }
        }
        System.out.println("Complete!");

        for (Test test : tests)
        {
            System.out.println(test.getName());
            for (Image img : images)
            {
                System.out.printf("%-16s %10d\n", img.getClass().getSimpleName(), timeTest(test, img));
            }
        }
    }
}
