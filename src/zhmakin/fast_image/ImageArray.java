package zhmakin.fast_image;

public class ImageArray
    implements Image
{
    private int width;
    private int height;
    private int[] rgb;

    public ImageArray(int width, int height)
    {
        this.width = width;
        this.height = height;
        this.rgb = new int[width * height];
    }


    @Override
    public int getRGB(int x, int y)
    {
        return this.rgb[y * width + x];
    }

    @Override
    public void setRGB(int x, int y, int rgb)
    {
        this.rgb[y * width + x] = rgb;
    }
}
