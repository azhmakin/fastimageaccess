package zhmakin.fast_image;

public class ImageArray2DYX
    implements Image
{
    private int[][] rgb;

    public ImageArray2DYX()
    {
        this.rgb = new int[1024][1024];
    }

    @Override
    public int getRGB(int x, int y)
    {
        return this.rgb[y][x];
    }

    @Override
    public void setRGB(int x, int y, int rgb)
    {
        this.rgb[y][x] = rgb;
    }
}
