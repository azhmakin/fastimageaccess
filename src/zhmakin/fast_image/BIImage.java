package zhmakin.fast_image;

import java.awt.image.BufferedImage;

public class BIImage
    implements Image
{
    private BufferedImage image;

    public BIImage()
    {
        this.image = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_ARGB);
    }

    @Override
    public int getRGB(int x, int y)
    {
        return this.image.getRGB(x, y);
    }

    @Override
    public void setRGB(int x, int y, int rgb)
    {
        this.image.setRGB(x, y, rgb);
    }
}
