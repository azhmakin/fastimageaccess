package zhmakin.fast_image;

public class ImageArray2DXY
    implements Image
{
    private int[][] rgb;

    public ImageArray2DXY()
    {
        this.rgb = new int[1024][1024];
    }

    @Override
    public int getRGB(int x, int y)
    {
        return this.rgb[x][y];
    }

    @Override
    public void setRGB(int x, int y, int rgb)
    {
        this.rgb[x][y] = rgb;
    }
}
